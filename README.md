About
=====

A simple CCD IK Solver for Unity. 

Implemented with two fixed bones, but should be easily extensible
to any number of bones.

![Sreenshot](screenshot.png)

License
=======

This software is released under the [UNLICENSE](http://unlicense.org/)
license, see the file UNLICENSE for more information.
