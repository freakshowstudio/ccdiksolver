﻿using UnityEngine;
using System.Collections;

namespace CCDIKSolver
{
    public class IKSolver : MonoBehaviour
    {
        #region Inspector Variables
        [Header("IK Chain")]
        [SerializeField] private Transform _root;
        [SerializeField] private Transform _elbow;
        [SerializeField] private Transform _wrist;

        [Header("IK Target")]
        [SerializeField] private Transform _target;

        [Header("Options")]
        [SerializeField] private float _toleranceSqr = 0.2f;
        [SerializeField] private int _maxNumTries = 5;

        [Header("Line Materials")]
        [SerializeField] private Material _lineMaterialFound;
        [SerializeField] private Material _lineMaterialNotFound;
        #endregion // Inspector Variables

        #region Private Variables
        private LineRenderer _rootElbowLine;
        private LineRenderer _elbowWristLine;
        #endregion // Private Variables

        #region Unity Methods
        void Awake()
        {
            GameObject rootElbow = new GameObject("Root-Elbow Line");
            GameObject elbowWrist = new GameObject("Elbow-Wrist Line");

            _rootElbowLine = rootElbow.AddComponent<LineRenderer>();
            _elbowWristLine = elbowWrist.AddComponent<LineRenderer>();

            _rootElbowLine.startWidth = 0.25f;
            _rootElbowLine.endWidth = 0.05f;

            _elbowWristLine.startWidth = 0.25f;
            _elbowWristLine.endWidth = 0.05f;

            _rootElbowLine.material = _lineMaterialFound;
            _elbowWristLine.material = _lineMaterialFound;
        }

        void Update()
        {
            bool found = SolveCCD();

            _rootElbowLine.SetPosition(0, _root.position);
            _rootElbowLine.SetPosition(1, _elbow.position);

            _elbowWristLine.SetPosition(0, _elbow.position);
            _elbowWristLine.SetPosition(1, _wrist.position);

            if (found)
            {
                _rootElbowLine.material = _lineMaterialFound;
                _elbowWristLine.material = _lineMaterialFound;
            }
            else
            {
                _rootElbowLine.material = _lineMaterialNotFound;
                _elbowWristLine.material = _lineMaterialNotFound;
            }
        }
        #endregion // Unity Methods

        #region Private Methods
        private bool SolveCCD()
        {
            bool found = false;

            Vector3 target = _target.position;

            for (int i = 0; i < _maxNumTries; ++i)
            {
                float d = (target - _wrist.position).sqrMagnitude;
                if (d <= _toleranceSqr)
                {
                    found = true;
                    break;
                }

                Vector3 lowArmDir =
                    (_wrist.position - _elbow.position).normalized;

                Vector3 lowArmTarget =
                    (target - _elbow.position).normalized;

                Quaternion lowArmRot =
                    Quaternion.FromToRotation(lowArmDir, lowArmTarget);

                _elbow.rotation = lowArmRot * _elbow.rotation;


                Vector3 upArmDir =
                    (_wrist.position - _root.position).normalized;

                Vector3 upArmTarget =
                    (target - _root.position).normalized;

                Quaternion upArmRot =
                    Quaternion.FromToRotation(upArmDir, upArmTarget);

                _root.rotation = upArmRot * _root.rotation;
            }
            return found;
        }
        #endregion // Private Methods
    }
}
